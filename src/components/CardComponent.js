import React from "react";
import { Card } from "primereact/card";
import { Button } from "primereact/button";
import { useHistory } from "react-router-dom";

const CardComponent = ({ img, title, text, to, buttonLabel }) => {
  let history = useHistory();
  const handleOnclick = () => {
    console.log("click en", to);
    history.push(`/${to}`);
  };
  const header = (
    <img style={{ width: "400px", height: "225px" }} alt="Card" src={img} />
  );
  const footer = (
    <span>
      <Button label={buttonLabel} icon="pi pi-check" onClick={handleOnclick} />
    </span>
  );

  return (
    <div className="borderApp">
      <Card
        title={title}
        style={{ width: "25em" }}
        footer={footer}
        header={header}
      >
        <p>{text}</p>
      </Card>
    </div>
  );
};

export default CardComponent;
