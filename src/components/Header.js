import React from "react";

const Header = () => {
  return (
    <header className="App-header">
      <h1>NILSON'S PIZZAS</h1>
    </header>
  );
};

export default Header;
