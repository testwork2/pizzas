import React, { useState, useEffect } from "react";
import { DataTable } from "primereact/datatable";
import { Column } from "primereact/column";
import { Link } from "react-router-dom";
import { srcApi } from "../config/variables";

const Sales = () => {
  const [salesList, setSalesList] = useState([]);

  const getSalesApi = () => {
    console.log("APIIIIIIIII");
    const url = `${srcApi}/orders`;
    fetch(url)
      .then((res) => res.json())
      .then((res) => {
        setSalesList(res);
      });
  };

  useEffect(() => {
    getSalesApi();
  }, []);

  return (
    <div>
      <h3>AQUÍ TENEMOS UN RESUMEN DE LAS ORDENES DE PIZZAS VENDIDAS</h3>
      <div className="p-p-6">
        <div className="card">
          <DataTable value={salesList}>
            <Column
              field="name_client"
              header="CLIENTE"
              sortable
              filter
            ></Column>
            <Column
              field="phone_client"
              header="TELÉFONO"
              sortable
              filter
            ></Column>
            <Column
              field="ingredients_list"
              header="INGREDIENTES"
              sortable
              filter
            ></Column>
            <Column
              field="pizza_name"
              header="NOMBRE PIZZA"
              sortable
              filter
            ></Column>
            <Column
              field="pizza_price"
              header="$ PRECIO"
              sortable
              filter
            ></Column>
            <Column
              field="order_date"
              header="FECHA DE COMPRA"
              sortable
              filter
            ></Column>
          </DataTable>
        </div>
      </div>
      <h3 className="p-pb-3">
        <Link to="/">Regresar al menú principal</Link>
      </h3>
    </div>
  );
};

export default Sales;
