import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Checkbox } from "primereact/checkbox";
import { Card } from "primereact/card";
import srcImgMasa from "../assets/masa.jpg";
import srcImgPre from "../assets/preparacion.jpeg";
import { InputText } from "primereact/inputtext";
import { Button } from "primereact/button";
import { Message } from "primereact/message";
import { srcApi } from "../config/variables";
import Spinner from "./Spinner";

const Orders = () => {
  const [ingredients, setIngredients] = useState([]);
  const [pizza, setPizza] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [ingredientsList, setIngredientsList] = useState("");
  const [clientName, setClientName] = useState("");
  const [phoneClient, setPhoneClient] = useState("");
  const [pizaName, setPizaName] = useState("");
  const [validated, setValidated] = useState(true);
  const [isLoading, setIsLoading] = useState(false);

  const getIngredientsApi = () => {
    console.log("APIIIIIIIII");
    setIsLoading(true);
    const url = `${srcApi}/ingredients`;
    fetch(url)
      .then((res) => res.json())
      .then((res) => {
        setIngredients(res);
        setIsLoading(false);
      });
  };

  // const ingredients = [
  //   { id: 1, nameIngredient: "pollo", price: 5000 },
  //   { id: 2, nameIngredient: "tomate", price: 3000 },
  //   { id: 3, nameIngredient: "queso", price: 3000 },
  //   { id: 4, nameIngredient: "jamón", price: 3000 },
  //   { id: 5, nameIngredient: "peperoni", price: 4500 }
  // ];

  useEffect(() => {
    let price = 10000;
    let totalList = "";
    pizza.forEach((item) => {
      price += ingredients.find((ingredient) => ingredient._id.$oid === item)
        .price;
      totalList += `${ingredients
        .find((ingredient) => ingredient._id.$oid === item)
        .name.toUpperCase()} - `;
    });
    //Hacemos el llamado al api toda vez que no hay ingredientes disponibles...
    if (ingredients.length <= 0) {
      getIngredientsApi();
    }

    setIngredientsList(totalList);
    setTotalPrice(price);
  }, [pizza]);

  const onIngredientChange = (e) => {
    let selectedIngredients = [...pizza];
    console.log("array.....", selectedIngredients);
    console.log("------------------", e.checked);
    console.log("+++++++++", selectedIngredients.indexOf(e.value));
    if (e.checked) selectedIngredients.push(e.value);
    else selectedIngredients.splice(selectedIngredients.indexOf(e.value), 1);
    setPizza(selectedIngredients);
  };

  const resetOrder = () => {
    setPizza([]);
    setTotalPrice(0);
    setIngredientsList("");
    setClientName("");
    setPhoneClient("");
    setPizaName("");
  };

  const handleClick = () => {
    //console.log("vamos a vender y a validar");
    //validaciones de los campos
    if (
      clientName.trim() === "" ||
      phoneClient.trim() === "" ||
      pizaName.trim() === "" ||
      pizza.length <= 0
    ) {
      setValidated(false);
      return;
    }
    setValidated(true);
    // Manejo de fecha
    let date = new Date();
    let day = date.getDate();
    let month = date.getMonth() + 1;
    let year = date.getFullYear();
    let order_date = "";

    if (month < 10) {
      order_date = `${year}-0${month}-${day}`;
    } else {
      order_date = `${year}-${month}-${day}`;
    }
    //llamado al api
    const body = {
      pizza_name: pizaName,
      pizza_price: totalPrice,
      name_client: clientName,
      phone_client: phoneClient,
      ingredients: pizza,
      ingredients_list: ingredientsList,
      order_date: order_date,
    };
    const url = `${srcApi}/orders`;
    setIsLoading(true);
    fetch(url, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(body),
    })
      .then((res) => res.json())
      .then((res) => {
        console.log("Aquí la respuesta", res);
        resetOrder();
        setTimeout(() => {
          setIsLoading(false);
        }, 5000);
      });
    console.log(body);
  };

  return (
    <div>
      {isLoading ? (
        <div className="p-mt-6 p-d-flex p-jc-center">
          <Spinner />
          <h2 className="p-p-6">Ordenando...</h2>
        </div>
      ) : (
        <div className="card">
          <h3>
            Aquí vamos preparando tu pizza personalizada con los ingredientes de
            tu preferencia...
          </h3>
          <div className="p-d-inline-flex">
            <div>
              <label>Ingredientes:</label>
              {ingredients.map((ingredient) => (
                <div
                  key={ingredient._id.$oid}
                  className="p-mr-2 p-mt-1 p-d-block p-text-left"
                >
                  <Checkbox
                    inputId={`${ingredient._id.$oid}`}
                    value={ingredient._id.$oid}
                    onChange={onIngredientChange}
                    checked={pizza.includes(ingredient._id.$oid)}
                  ></Checkbox>
                  <label
                    htmlFor={`${ingredient._id.$oid}`}
                    className="p-d-inline p-checkbox-label p-ml-2"
                  >
                    {`${ingredient.name.toUpperCase()}- $ ${ingredient.price}`}
                  </label>
                </div>
              ))}
            </div>
            <div className="p-ml-6 border">
              <Card
                title={
                  pizza.length > 0 ? "PIZZA EN PREPARACIÓN" : "MASA PARA PIZZA"
                }
                style={{ width: "25rem", marginBottom: "2em" }}
              >
                <img
                  style={{ width: "320px", height: "188px" }}
                  alt="ImgCard"
                  src={pizza.length > 0 ? srcImgPre : srcImgMasa}
                />
                <p className="p-m-5" style={{ lineHeight: "1.5" }}>
                  {pizza.length > 0
                    ? ingredientsList
                    : "Aún no agregó ingredientes"}
                </p>
              </Card>
            </div>
          </div>
          <h1>Precio actual: {`$ ${totalPrice}`}</h1>
          <div className="p-d-inline-flex">
            <span className="p-float-label p-m-2">
              <InputText
                id="clientName"
                value={clientName}
                onChange={(e) => setClientName(e.target.value)}
              />
              <label style={{ color: "#000" }} htmlFor="clientName">
                Nombre del cliente
              </label>
            </span>
            <span className="p-float-label p-m-2">
              <InputText
                id="phoneClient"
                value={phoneClient}
                onChange={(e) => setPhoneClient(e.target.value)}
              />
              <label style={{ color: "#000" }} htmlFor="phoneClient">
                Teléfono del cliente
              </label>
            </span>
            <span className="p-float-label p-m-2">
              <InputText
                id="pizaName"
                value={pizaName}
                onChange={(e) => setPizaName(e.target.value)}
              />
              <label style={{ color: "#000" }} htmlFor="pizaName">
                Nombre de la pizza
              </label>
            </span>
          </div>
          <div className="p-mt-2">
            <Button label="Ordenar Pizza" onClick={handleClick} />
          </div>
          {validated ? (
            ""
          ) : (
            <Message
              className="p-mt-3"
              severity="warn"
              text="Los tres campos son obligatorios y debe seleccionar por lo menos un
        ingrediente."
            ></Message>
          )}
        </div>
      )}
      <h3 className="p-pb-3">
        <Link to="/">Regresar al menú principal</Link>
      </h3>
    </div>
  );
};

export default Orders;
