import React from "react";
import CardComponent from "./CardComponent";
import srcImg from "../assets/pizza.jpg";
import salesImg from "../assets/venta.jpg";

const Menu = () => {
  return (
    <div className="p-mt-4 p-d-flex p-jc-center p-pb-6">
      <div className="p-mr-2 p-d-flex p-jc-center">
        <CardComponent
          img={srcImg}
          title="Vamos a crear una pizza!"
          text="Aqui podemos crear una pizza con cualquiera de nuestros ingredientes..."
          to="orders"
          buttonLabel="Ordenar"
        />
      </div>
      <div className="p-mr-2 p-d-flex p-jc-center">
        <CardComponent
          img={salesImg}
          title="Vamos a revisar nuestras ventas!"
          text="Aqui podemos tener un recuento de las pizzas vendidas en Nilson's Pizzas..."
          to="sales"
          buttonLabel="Ventas"
        />
      </div>
    </div>
  );
};

export default Menu;
